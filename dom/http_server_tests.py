# -*- encoding: utf-8 -*-

import unittest
from http_server_tests_method import Connect


# TESTY wymagają uruchomionego servera (zdalnie lub lokalnie).
# Przy uruchomieniu lokalnym należy zmienić wartość zmiennej self.server_address w pliku http_server_tests_method.py
#   na odpwiadającą lokalnemu adresowi serwera, by testy zadziałały.
class HttpServerTestCase(unittest.TestCase):
    # METODA - Sprawdzenie reakcji na niedozwolony typ żądania
    def test_method_not_allowed1(self):
        connect = Connect()
        methods = ['POST', 'PUT', 'DELETE', 'HEAD']
        for method in methods:
            header = connect.connect(connect.return_header(method, '/', 'HTTP/1.1'))[0]
            self.assertEqual('HTTP/1.1 405 Method Not Allowed', header)

    # METODA - Sprawdzenie reakcji na niedozwolony protokół żądania
    def test_method_not_allowed2(self):
        connect = Connect()
        protocols = ['FTP', 'SIP', 'SMTP', 'IMAP']
        for protocol in protocols:
            header = connect.connect(connect.return_header('GET', '/', protocol))[0]
            self.assertEqual('HTTP/1.1 405 Method Not Allowed', header)

    # METODA - Sprawdzenie reakcji na nieprawidłowe żądanie
    def test_method_not_allowed3(self):
        connect = Connect()
        header = connect.connect(connect.return_header('car34tg4bny', ' fsdafas ffagds', '.,.;pqwe.śźćw'))[0]
        self.assertEqual('HTTP/1.1 405 Method Not Allowed', header)

    # METODA - Sprawdzenie reakcji na właściwe żądanie dostępu do folderu
    def test_200a(self):
        connect = Connect()
        header = connect.connect(connect.return_header('GET', '/', 'HTTP/1.1'))[0]
        self.assertEqual('HTTP/1.1 200 OK', header)

    # METODA - Sprawdzenie reakcji na właściwe żądanie dostępu do pliku
    def test_200b(self):
        connect = Connect()
        header = connect.connect(connect.return_header('GET', '/lokomotywa.txt', 'HTTP/1.1'))[0]
        self.assertEqual('HTTP/1.1 200 OK', header)

    # METODA - Sprawdzenie reakcji na właściwe żądanie dostępu do nieistniejącego uri
    def test_400(self):
        connect = Connect()
        header = connect.connect(connect.return_header('GET', '/abc', 'HTTP/1.1'))[0]
        self.assertEqual('HTTP/1.1 404 Not Found', header)

    # METODA - Sprawdzenie zawartośći nagłówków
    def test_headers(self):
        connect = Connect()
        headers = connect.connect(connect.return_header('GET', '/', 'HTTP/1.1'))
        if 'HTTP/1.1 200 OK' == headers[0] \
                and 'content-type' in headers[1].lower() \
                and 'content-length' in headers[2].lower():
            passed = True
        else:
            passed = False

        self.assertEqual(True, passed)
        self.assertEqual(3, len(headers))

    # METODA - Sprawdzenie zawartości nagłówka content-type w zależności od żądanego uri
    def test_content_type(self):
        connect = Connect()
        cts = ['/', '/web_page.html', '/lokomotywa.txt', '/images/gnu_meditate_levitate.png', '/images/jpg_rip.jpg']
        type_of_contents = ['text/html', 'text/html', 'text/plain', 'image/png', 'image/jpeg']

        content_type = []
        i = 0
        for content in cts:
            header = connect.connect(connect.return_header('GET', content, 'HTTP/1.1'))[1]
            content_type.append(header.split(' ')[1].replace(';', ''))
            self.assertEqual(content_type[i], type_of_contents[i])
            i += 1

if __name__ == '__main__':
    unittest.main()
