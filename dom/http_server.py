# -*- encoding: utf-8 -*-

import socket
from http_server_methods import create_and_send_response, create_error_response


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 34567)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)
print 'Serwer nasłuchuje połączenia!'

try:
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        try:
            # Odebranie żądania
            request = connection.recv(2048)

            # Przetwarzanie żądania i wysłanie zawartości strony
            if request:
                print request
                create_and_send_response(connection, request)
        except:
            try:
                # Wysłanie komunikatu o błędzie na serwerze (500)
                error_500 = create_error_response('500 Internal Server Error')
                connection.sendall(error_500)
            except socket.error:
                print u'Nie udało się wysłać powiadomienia o błędzie do ', client_address
        finally:
            # Zamknięcie połączenia
            connection.close()
except KeyboardInterrupt:
    # Zamknięcie socketa
    server_socket.close()