# -*- encoding: utf-8 -*-

import os
import mimetypes


# METODA - Tworzenie i wysyłanie odpowiedzi
def create_and_send_response(conn, reqt):
    #Parsowanie żądania
    status_header = reqt.split('\r\n')[0]
    address = 'web' + reqt.split(' ')[1].replace('%20', ' ')

    # Sprawdzenie typu i protokołu żadania
    if 'HTTP' in status_header and 'GET' in status_header:
        # Sprawdzenie czy podany adres istnieje
        if os.path.exists(address):
            # Sprawdzenie czy żadanie dotyczy konkretnego pliku czy folderu
            if os.path.isdir(address):
                # Tworzenie strony eksploratora
                page = create_directory_page(address)
                content_type = 'text/html'
            else:
                # Otworzenie konkretnego pliku
                page = open(address, 'rb').read()
                content_type = mimetypes.guess_type(address)[0]
            # Tworzenie nagłówka i łączenie go z zawartością strony
            header = create_header('200 OK', content_type, len(page))
            complete_page = header + page
        else:
            # Tworzenie strony 404
            complete_page = create_error_response('404 Not Found')
    else:
        # Tworzenie strony 405
        complete_page = create_error_response('405 Method Not Allowed')

    # Wysłanie strony do klienta
    conn.sendall(complete_page)


# METODA - Tworzenie strony eksploratora
def create_directory_page(address):
    # Szablon strony
    template = '<!DOCTYPE html><html><head><meta charset="utf-8"/></head>' \
               '<body><h1>Folder %s:</h1><hr></br>%s</body></html>'

    # Generowanie odpowiednich linków dla folderów i plików
    links = ''
    for i in os.listdir(address):
        if address != 'web/':
            source_address = address + '/' + i
        else:
            source_address = address + i

        links += '<p style="margin: 10px;"><a style="font-size:18px" href="%s">%s</a></li>' \
                 % (source_address.lstrip('web'), i)

    # Tworzenie i zwracanie gotowej strony
    return template % (address.rstrip('/'), links)


# METODA - Zwracanie kompletnej strony błędu
def create_error_response(status):
    content_type = 'text/html'
    page = create_page(status)
    return create_header(status, content_type, len(page)) + page


# METODA - Zwracanie tylko strony html
def create_page(status):
    page = '<!DOCTYPE html><html><head><meta charset="utf-8"/></head><body><h1>%s</h1></body></html>'
    return page % status


# METODA - Zwracanie tylko nagłówka
def create_header(status, content_type, content_lenght):
    header = 'HTTP/1.1 %s\r\nContent-Type: %s; charset=UTF-8\r\nContent-Length: %s\r\n\r\n'
    return header % (status, content_type, str(content_lenght))