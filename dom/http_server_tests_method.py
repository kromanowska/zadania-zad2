# -*- encoding: utf-8 -*-

import socket


class Connect():
    # KONSTRUKTOR - Inicjalizacja zminnej server_address
    def __init__(self):
        self.server_address = ('194.29.175.240', 34567)  # TODO: zmienić port!

    # METODA - Wysyłanie ządania, odbieranie i zwracanie odpowiedzi
    def connect(self, header):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.connect(self.server_address)

        try:
            server_socket.sendall(header)
            server_response = server_socket.recv(1024)
        except socket.error:
            return ''
        finally:
            server_socket.close()

        return server_response[:server_response.find('\r\n\r\n')].split('\r\n')

    # METODA - Tworzenie i zwracanie nagłówka
    def return_header(self, connection_type, connection_uri, connection_protocol):
        server_address = '%s:%s' % (self.server_address[0], str(self.server_address[1]))
        header = '%s %s %s\r\nHost: %s\r\nConnection: keep-alive\r\n\r\n'
        return header % (connection_type, connection_uri, connection_protocol, server_address)