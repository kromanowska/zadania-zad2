# -*- coding: utf-8 -*-
import smtplib

def create_and_send_email():
    """Funkcja interaktywna pytająca użytkownika o potrzebne dane
    i wysyłająca na ich podstawie list elektroniczny.
    """
    # Odczyt danych od użytkownika
    nadawca = raw_input('Od: ')
    odbiorca = raw_input('Do: ')
    subject = raw_input('Temat: ')
    message = raw_input('Wiadomosc: ')
    # Stworzenie wiadomości
    wiadomosc = 'From ' + nadawca + '\r\n'+'To ' + odbiorca + '\r\n'+'Subject '+ subject +'\r\n\r\n'+ message

    try:
        # Połączenie z serwerem pocztowym
        server = smtplib.SMTP('194.29.175.240', 25)
        server.set_debuglevel(True)

        # Ustawienie parametrów
        server.starttls()
        server.ehlo()
        # Autentykacja
        server.login('p2', 'p2')
        # Wysłanie wiadomości
        server.sendmail(nadawca,odbiorca,wiadomosc)

    finally:
        # Zamknięcie połączenia
        server.close()
        pass


if __name__ == '__main__':
    decision = 't'
    while decision in ['t', 'T', 'y', 'Y']:
        create_and_send_email()
        decision = raw_input('Wysłać jeszcze jeden list? ')
